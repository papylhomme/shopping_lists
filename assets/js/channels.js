import Vue from 'vue'
import { v1 as UUIDv1 } from 'uuid';

import socket from "./socket"


/**
 * Mixin designed to help with phoenix channels
 *
 * In order to provide a channel, you can override the computed value '_channel' (this is used by the other channel mixins)
 *
 * Parameters: {handlers}
 * - handlers: a list of event to subscribe to
 */
export function usingChannel(params) {
  let config = params || {}
  return {
    // Setup internal data
    data: function() { return {
      _channels: {
        handlers: [],
        commands: {}
      }
    }},

    computed: {
      // The actual channel object. Override this to provide a channel to the mixin
      _channel: function() { return null },


      /*
       * Helper function to test if a command is running
       *
       * If no name is given, test if any command is running.
       * If params are given, test if a command with the givens is running.
       */
      commandRunning: function() { return (name, params) => {
        if(name == null && params == null) {
          return Object.keys(this._data._channels.commands).length > 0

        } else if(params == null) {
          return Object.values(this._data._channels.commands).find(({command}) => command == name) != null

        } else {
          return Object.values(this._data._channels.commands).find(cmd => {
            if(name != null && cmd.command != name) return false
            return Object.entries(params).reduce((acc, [key, value]) => acc && cmd.params[key] == value, true)
          }) != null
        }
      }}
    },

    watch: {
      // Watch for channel change and setup/clean accordingly
      _channel: function(v, ov) {
        if(v == null && ov != null) {
          this._channelClean(ov)

        } else if(v != null && v != ov) {
          this._channelClean(ov)
          this._channelSetup(v)
        }
      }
    },


    created: function() {
      // If _channel is present at creation time, set it up directly
      // as the watch for _channel change won't be triggered
      if(this._channel) this._channelSetup(this._channel)
    },


    // Clean channel on destroy
    beforeDestroy: function() {
      this._channelClean(this._channel)
    },

    methods: {

      // Channel lifecycle hooks
      onChannelSetup(channel) {},
      onChannelCleanup(channel) {},
      onChannelEvent(type, payload) {},

      // Send a command through the channel
      channelCommand(command, params, ok, error, timeout) {
        if(this._channel == null) {
          console.error(this.$t('command.error'), 'channel_not_set')
          return
        }

        console.debug(`Sending channel command ${command}`)
        let id = UUIDv1()
        let cmd = {
          command: command,
          params: params
        }

        Vue.set(this._data._channels.commands, id, cmd)

        this._channel.push(command, params)
          // Success handler
          .receive("ok", resp => {
            Vue.delete(this._data._channels.commands, id)
            if(ok) ok(resp)
          })
          // Error handler
          .receive("error", resp => {
            Vue.delete(this._data._channels.commands, id)
            if(error) error(resp)
            else console.error(this.$t('command.error'), resp.reason)
          })
          // Timeout handler
          .receive("timeout", () => {
            Vue.delete(this._data._channels.commands, id)
            if(timeout) timeout()
            else console.error(this.$t('command.timeout'))
          })

      },

      // Setup handlers
      _channelSetup: function(channel) {
        if(channel == null) return

        console.debug('Setup channel')
        this._data._channels.handlers = []
        if(Array.isArray(config.handlers)) {
          this._data._channels.handlers = config.handlers.map(event => {
            let ref = channel.on(event, payload => this.onChannelEvent(event, payload))
            return {ref: ref, event: event}
          })
        }

        this.onChannelSetup(channel)
      },

      // Clean handlers
      _channelClean: function(channel) {
        if(channel == null) return

        console.debug('Cleaning channel')
        this.onChannelCleanup(channel)
        this._data._channels.handlers.forEach(handler => channel.off(handler.event, handler.ref))
        this._data._channels.handlers = []
      },

    },
  }
}



/**
 * Mixin designed to help setting up the connexion to a channel
 *
 * This module use usingChannel under the hood, overriding '_channel' with the managed one.
 * You can override the computed value '_channelName' for a dynamic channel name
 *
 * Parameters: {channelName}
 * - channelName: The name of the channel to join
 *
 * See usingChannel for a description of the other parameters
 */
export function withChannel(params) { return {

  // Base channel functionality
  mixins: [ usingChannel(params) ],

  // Data for the managed channel
  data: function() { return {
    _channels: {
        channel: null
    }
  }},

  computed: {
    // Setup the channel for usingChannel
    _channel: function() { return this._data._channels.channel },

    // Default channel name from params. Override for dynamic channel names
    _channelName: function() { return params ? params.channelName : null },

    // Default channel join params. Override to pass dynamic parameters
    _channelJoinParams: function() { return params ? params.joinParams : null }
  },

  watch: {

    // Watch channel name and leave/join channels accordingly
    _channelName: function(v, ov) {
      if(v == null && ov != null) {
        this._channelLeave(this._channel)

      } else if(v != null && v != ov) {
        this._channelLeave(this._channel)
        this._channelJoin(v)
      }
    }
  },

  // Setup channel on initialization
  created() {
    if(this._channelName) this._channelJoin(this._channelName)
  },

  // Leave channel on destroy
  beforeDestroy() {
    this._channelLeave(this._channel)
  },

  methods: {

    // Handle channel joined
    onChannelJoined(channel, resp) { },

    // Leave channel
    _channelLeave(channel) {
      if(this._channel == null) return

      console.debug('Leaving channel')
      this._channel.leave()
      this._data._channels.channel = null
    },

    // Join channel
    _channelJoin(channelName) {
      if(channelName == null) return

      console.debug(`Joining channel ${channelName}`)
      let instance = this
      let channel = socket.channel(channelName, this._channelJoinParams)
      channel.join()
        .receive("ok", (res) => {
          this._data._channels.channel = channel
          this.onChannelJoined(channel, res)
        })
        .receive("error", ({reason}) => console.error(`Error joining channel: ${reason}`))
        .receive("timeout", () => console.error("Timeout joining channel"))

      channel.onClose(() => console.debug('Closing channel'))
      channel.onError((error) => console.error(`Channel error: ${JSON.stringify(error)}`))
    }
  }

} }
