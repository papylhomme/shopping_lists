import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)


import App from './app.vue'
import Lists from './lists.vue'
import List from './list.vue'

const router = new VueRouter({
  routes: [
    { path: '/', component: App, children: [
      { path: '/', redirect: 'lists' },
      { path: 'lists', component: Lists },
      { path: 'list/:list', component: List, props: (route) => ({listId: Number.parseInt(route.params.list)}) },
    ]},
  ]
})

export default router
