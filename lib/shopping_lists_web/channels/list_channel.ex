defmodule ShoppingListsWeb.ListChannel do
  use ShoppingListsWeb, :channel
  import Ecto.Query, only: [from: 2]

  require Logger
  alias ShoppingLists.{Repo, Item}

  # JOIN: retrieve the items on join
  def join("list:" <> list_id, payload, socket) do
    if authorized?(payload) do
			{id, ""} = Integer.parse(list_id)
      items = Repo.all(from i in Item, where: i.list_id == ^id, select: i)
      {:ok, items, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # IN: create a new item
  def handle_in("create_item", %{"name" => name, "list_id" => list_id}, socket) do
    name = String.trim(name)
    Logger.debug "Creating item #{name} in list #{list_id}"

    case Repo.insert(%Item{name: name, list_id: list_id}) do
      {:error, changeset} -> Logger.error "Error creating item: #{inspect changeset}"
      {:ok, item} ->
        broadcast socket, "item_created", item
        update_item_count(item.list_id)
    end

    {:reply, :ok, socket}
  end

  # IN: update an item
  def handle_in("update_item", %{"id" => id} = payload, socket) do
    Logger.debug "Updating item #{id} with #{inspect payload}"

    case Repo.get(Item, id) do
      nil -> Logger.warn "Cannot update unknown item #{id}"
      item ->
        item =
          item
          |> Ecto.Changeset.change(name: payload["name"] || item.name)
          |> Ecto.Changeset.change(checked: Map.get(payload, "checked", item.checked))

        case Repo.update(item) do
          {:error, changeset} -> Logger.error "Error updating item: #{inspect changeset}"
          {:ok, item} ->
            broadcast socket, "item_updated", item
            update_item_count(item.list_id)
        end
    end

    {:reply, :ok, socket}
  end

  # IN: delete an item
  def handle_in("delete_item", %{"id" => id}, socket) do
    Logger.debug "Deleting item #{id}"

    case Repo.get(Item, id) do
      nil -> Logger.warn "Cannot delete unknown item #{id}"
      item ->
        Repo.delete(item)
        broadcast socket, "item_deleted", item
        update_item_count(item.list_id)
    end

    {:reply, :ok, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end

  # Count item and broadcast the result
  defp update_item_count(list_id) do
    count = Repo.one(from i in Item, where: i.checked == false and i.list_id == ^list_id, select: count(i.id))
    ShoppingListsWeb.Endpoint.broadcast("main", "list_updated", %{id: list_id, items: count})
  end

end

require Protocol
Protocol.derive(Jason.Encoder, ShoppingLists.Item, only: [:id, :name, :checked, :list_id])
